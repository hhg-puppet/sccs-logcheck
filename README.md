# logcheck

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with logcheck](#setup)
    * [What logcheck affects](#what-logcheck-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with logcheck](#beginning-with-logcheck)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
6. [Limitations - OS compatibility, etc.](#limitations)

## Overview

This module configures logcheck on Linux based systems.

## Module Description

This module manages installation and configuration of logcheck on Linux based systems.

## Setup

### What logcheck affects

* Creates a ignore.d.server rule file "puppet-custom" in ignore.d.server folder.
* Changes SENDMAIL attribute in logcheck.conf.

### Setup Requirements

* You must have a proper MTA installed and configured to send mail externally.
* You must have augeas libraries installed.

### Beginning with logcheck

See [Usage](#usage)

## Usage

### Using default values

	class { '::logcheck': }

### Using available parameters
	
	class { '::logcheck':
   		rules   => "#Your logcheck rules#",
		email   => 'test@test.com',
	}

## Reference

### Public Classes

* logcheck: Main class, includes all other classes.

### Private Classes

* logcheck::install: Handles the packages.
* logcheck::config: Handles the configuration file and logcheck rules

### Parameters

#### `rules`

#### `email`

## Limitations

This module has been built on and tested against Puppet 3.

The module has beent tested on Ubuntu 12.04/14.04 LTS and Debian Jessie.
