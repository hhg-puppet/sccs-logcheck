# == Class: logcheck
#
# See README.md for details
#
# === Authors
#
# Daniel Fomin <daniel.fomin@icloud.com>
#
# === Copyright
#
# Copyright 2016 Technical University of Munich
#
class logcheck 
(
	$rules,
	$email = logcheck
)

{
	include logcheck::install
	include logcheck::config
}
