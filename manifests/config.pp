# == Class: logcheck::config
#
# See README.md for details
#
# === Authors
#
# Daniel Fomin <daniel.fomin@icloud.com>
# Sebastian Rettenberger <sebastian.rettenberger@tum.de>
#
# === Copyright
#
# Copyright 2016 Technical University of Munich
#
class logcheck::config inherits logcheck
{
	# Workaround for Foreman using Windows newlines
	$rules_unix = regsubst($logcheck::rules, '\r\n', "\n", 'G')

	file {'/etc/logcheck/ignore.d.server/puppet-custom':
		ensure => 'present',
		owner => 'root',
		content => "# MANAGED BY PUPPET!\n\n${rules_unix}",
	}

	augeas { 'logcheck':
		incl => '/etc/logcheck/logcheck.conf',
		lens => 'Shellvars.lns',
		context => '/files/etc/logcheck/logcheck.conf',
		changes => "set SENDMAILTO '\"${logcheck::email}\"'",
		require => Package['logcheck']
	}
	
}
