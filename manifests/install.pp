# == Class: logcheck::install
#
# See README.md for details
#
# === Authors
#
# Daniel Fomin <daniel.fomin@icloud.com>
#
# === Copyright
#
# Copyright 2016 Technical University of Munich
#
class logcheck::install inherits logcheck {

	package {'logcheck':
		ensure => 'installed'
	}
    
}
